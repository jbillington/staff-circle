﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace SendAndRecieveSMS.Models
{
    public class FullSmsDetailModel
    {
        public string sid { get; set; }
        DateTime? TimeSent { get; set; }
        public string MessageBody { get; set; }
        public PhoneNumber FromNumber { get; set; }
        public string ToNumber { get; set; }
        public DateTime? DataSent { get; internal set; }
        public MessageResource.StatusEnum MessageStatus { get; internal set; }

        public class FullSmsDetailList
        {
            public List<FullSmsDetailModel> SmsMessages { get; set; }
        }
    }
}