﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SendAndRecieveSMS.Models
{
    public class SendMessageModel
    {
        [Required(ErrorMessage = "You must provide a message")]
        public string MyMessage { get; set; }
    }
}