﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

using Twilio.TwiML;
using Twilio.AspNet.Mvc;
using SendAndRecieveSMS.Models;

namespace SendAndRecieveSMS.Controllers
{
    public class SmsController : Controller
    {

        public string To => ConfigurationManager.AppSettings["MyNumber"];
        private PhoneNumber From { get; } = new PhoneNumber("+447492881517");

        public string accountSid => ConfigurationManager.AppSettings["TwilioAccountSid"];
        public string authToken => ConfigurationManager.AppSettings["TwilioAuthToken"];

        // GET: Sms
        public ActionResult SendMySms(SendMessageModel model)
        {
            
            TwilioClient.Init(accountSid, authToken);

            var myMessageToSend = (MessageResource)null;

            if (ModelState.IsValid)
            {
                //get the users message
                string getMyMessageToSend = MyHelpers.PageHelpers.HtmlRawCleanup(model.MyMessage);

                //check the length of the string first.
                int count = 0;
                foreach (char c in getMyMessageToSend)
                {
                    count++;
                }

                if(count > 160)
                {
                    TempData["ErrMsg"] = "Sorry, You're only allow to send up to 160 characters. Your current character count is: " + count;
                    return View("~/Views/Home/Index.cshtml");
                }
                else
                {
                    //send message to mobile
                    myMessageToSend = MessageResource.Create(
                    to: To,
                    from: From,
                    body: getMyMessageToSend);
                    TempData["SmsSent"] = "Your message has been sent, your reference No. is: " + myMessageToSend.Sid;
                }

               
            }
            else
            {
                TempData["ErrMsg"] = "Sorry, you need to provide a message...";
                return View("~/Views/Home/Index.cshtml");
            }
            return View("~/Views/Home/Index.cshtml");
        }
    
        
    }
}