﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

using Twilio.TwiML;
using Twilio.AspNet.Mvc;
using SendAndRecieveSMS.Models;
using static SendAndRecieveSMS.Models.FullSmsDetailModel;
using System.Data;

namespace SendAndRecieveSMS.MyHelpers
{
    
    public static class PageHelpers
    {
        /// <summary>
        /// Clean html raw string and remove any html markup.
        /// </summary>
        /// <param name="data"></param>
        /// <remarks>pass in a html raw markup string to clean and remove and unnecessary characters</remarks>
        /// <returns></returns>
        public static string HtmlRawCleanup(string data)
        {
            string text = data;

            string clean = text
                .Replace("<br />", " ")
                .Replace("<div>", "")
                .Replace("</div>", "")
                .Replace("<h2>", "")
                .Replace("<h3>", "")
                .Replace("<table>", "")
                .Replace("</table>", "")
                .Replace("<tbody>", "")
                .Replace("</tbody>", "")
                .Replace("<tr>", "")
                .Replace("</tr>", "")
                .Replace("<td>", "")
                .Replace("</td>", "")
                .Replace("<strong>", "")
                .Replace("</strong>", "")
                .Replace("class", "")
                .Replace("<span>", "")
                .Replace("</span>", "")
                .Replace("</h2>", "")
                .Replace("</h3>", "")
                .Replace("<p>", "")
                .Replace("</p>", "")
                .Replace("<ul>", "")
                .Replace("</ul>", "")
                .Replace("<li>", "")
                .Replace("</li>", "");
            string shortDescription = clean.Contains("<img") ? Regex.Replace(clean, @"(<img\/?[^>]+>)", @"", RegexOptions.IgnoreCase) : clean;
            string shortDescriptionSet01 = shortDescription.Contains("<iframe") ? Regex.Replace(clean, @"(<iframe\/?[^>]+>)", @"", RegexOptions.IgnoreCase) : shortDescription;
            string shortDescriptionRemoveTablesSet02 = shortDescriptionSet01.Contains("<table") ? Regex.Replace(clean, @"(<table\/?[^>]+>)", @"", RegexOptions.IgnoreCase) : shortDescriptionSet01;

            string cleanString = shortDescriptionRemoveTablesSet02;

            return cleanString;
        }

        public static FullSmsDetailList GetMyMessages()
        {
            string to = System.Configuration.ConfigurationManager.AppSettings["MyNumber"];
            string from = ("+447492881517");

            string accountSid = System.Configuration.ConfigurationManager.AppSettings["TwilioAccountSid"];
            string authToken = System.Configuration.ConfigurationManager.AppSettings["TwilioAuthToken"];

            TwilioClient.Init(accountSid, authToken);

            var messages = MessageResource.Read();

            List<FullSmsDetailModel> messagesList = new List<FullSmsDetailModel>();

            foreach (var sms in messages)
            {
                messagesList.Add(new FullSmsDetailModel
                {
                    sid = sms.Sid,
                    FromNumber = sms.From,
                    ToNumber = sms.To,
                    MessageBody = sms.Body,
                    DataSent = sms.DateSent,
                    MessageStatus = sms.Status
                });
            }


            return new FullSmsDetailList
            {

                SmsMessages = messagesList

            };
        }

    }
}